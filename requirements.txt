################################################################################
# This requirements file has been automatically generated from `Pipfile` with
# `pipenv-to-requirements`
#
#
# This has been done to maintain backward compatibility with tools and services
# that do not support `Pipfile` yet.
#
# Do NOT edit it directly, use `pipenv install [-d]` to modify `Pipfile` and
# `Pipfile.lock` and then regenerate `requirements*.txt`.
################################################################################

appdirs==1.4.4
black==20.8b1
certifi==2020.12.5
chardet==4.0.0 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
click==7.1.2 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
django-countries==7.0
django-dotenv==1.4.2
django-seed==0.2.2
django-smtp-ssl==1.0
django==2.2.5
faker==5.3.0 ; python_version >= '3.6'
flake8==3.8.4
idna==2.10 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
mccabe==0.6.1
mypy-extensions==0.4.3
pathspec==0.8.1
pillow==8.1.0
pycodestyle==2.6.0 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
pyflakes==2.2.0 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
python-dateutil==2.8.1 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
python-gettext==4.0
pytz==2020.5
regex==2020.11.13
requests==2.25.1
six==1.15.0 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
sqlparse==0.4.1 ; python_version >= '3.5'
text-unidecode==1.3
toml==0.10.2 ; python_version >= '2.6' and python_version not in '3.0, 3.1, 3.2, 3.3'
typed-ast==1.4.2
typing-extensions==3.7.4.3
psycopg2==2.8.6
urllib3==1.26.2 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4' and python_version < '4'
sentry-sdk==0.19.5
